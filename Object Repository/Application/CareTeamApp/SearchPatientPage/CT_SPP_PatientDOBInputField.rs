<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CT_SPP_PatientDOBInputField</name>
   <tag></tag>
   <elementGuidId>41af07a7-bafd-45c8-8cf6-77639ed4019a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'dateOfBirth_dateTextField']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>dateOfBirth_dateTextField</value>
   </webElementProperties>
</WebElementEntity>
