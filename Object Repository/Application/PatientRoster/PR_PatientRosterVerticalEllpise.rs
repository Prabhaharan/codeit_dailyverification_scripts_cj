<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PR_PatientRosterVerticalEllpise</name>
   <tag></tag>
   <elementGuidId>91deb71a-a67c-4f7c-a6cb-fbd71a71d16d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(text(),'Insights &amp; Actions')]/following::div[@class='patient-item']//div[contains(text(),'386827')]/following::button/following::i[text()='more_vert']</value>
   </webElementProperties>
</WebElementEntity>
