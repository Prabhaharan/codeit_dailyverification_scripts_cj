<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>carePlanActivityStartDate1</name>
   <tag></tag>
   <elementGuidId>3cc895ef-281f-4d4b-a047-f6c1f6364c01</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[text()='Careplan Activity Start Date']/following::input[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Careplan Activity Start Date']/following::input[3]</value>
   </webElementProperties>
</WebElementEntity>
