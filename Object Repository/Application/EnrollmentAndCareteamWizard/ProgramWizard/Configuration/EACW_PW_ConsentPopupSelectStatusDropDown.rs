<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_PW_ConsentPopupSelectStatusDropDown</name>
   <tag></tag>
   <elementGuidId>fd49acbd-fc12-4b41-848e-9693aa46e4fe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//md-select-value/span/div[contains(text(),&quot;Select Status&quot;)]/..//following-sibling::span</value>
   </webElementProperties>
</WebElementEntity>
