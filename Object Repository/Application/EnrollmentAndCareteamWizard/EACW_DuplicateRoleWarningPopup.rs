<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_DuplicateRoleWarningPopup</name>
   <tag></tag>
   <elementGuidId>606acc67-9661-40db-8802-e7a23c5ee0e6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[text()='Warning!']/following::div[text()='Only the below roles can be duplicated:']</value>
   </webElementProperties>
</WebElementEntity>
