<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>goalTypeValue</name>
   <tag></tag>
   <elementGuidId>da31cda9-3afc-4f37-9a1e-3fafcb34d13a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//option[text()='Long Term']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//option[text()='Long Term']</value>
   </webElementProperties>
</WebElementEntity>
