<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>okButtonInConfirmPopup</name>
   <tag></tag>
   <elementGuidId>88afe54e-e062-43ae-a4ea-8de32effc220</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id='ATLC_OK_but']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id='ATLC_OK_but']</value>
   </webElementProperties>
</WebElementEntity>
