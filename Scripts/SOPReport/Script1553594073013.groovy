import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.relevantcodes.extentreports.ExtentReports
import com.relevantcodes.extentreports.LogStatus
import com.relevantcodes.extentreports.ExtentTest
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.exception.StepErrorException
import org.apache.commons.lang.StringUtils;


ExtentTest test=GlobalVariable.testrep
try{
	if(GlobalVariable.testRun.contains("Y")){
		GlobalVariable.runStatus = false
		
		    // Navigate to population manager app
			WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/Common/Pop_Mgr_icon'), 5, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(2)
			WebUI.click(findTestObject('Application/PopulationManagerApp/Common/Pop_Mgr_icon'), FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(2)
			WebUI.switchToWindowIndex(1)
			WebUI.delay(2)
			WebUI.setViewPortSize(1920, 1080)
			WebUI.delay(4)
			
			//Standard Operation Report Navigation
			WebUI.click(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_HomeButton'), FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(4)
			WebUI.mouseOver(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_DemoBrowseButton'), FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_DemoBrowseButton'), FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementClickable(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_PublicButton'), 160, FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_Public_ExpandButton'), FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_SOPFolder'), 6, FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_SOPFolder'), FailureHandling.STOP_ON_FAILURE)
			Boolean StandardOperationReportNavigation=WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_SOPFolder'), 3, FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_SOPFolder'), 6, FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/PMA_SFM_SOPFolder'),FailureHandling.STOP_ON_FAILURE)

			// Navigate to Program Enrollment Report
			WebUI.doubleClick(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/ProgramEnrollmentReport'), FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30, FailureHandling.STOP_ON_FAILURE)
			WebUI.switchToFrame(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30,FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/FromDate'),10, FailureHandling.OPTIONAL)
			WebUI.delay(3)
			
             //Click Parent Program
			 TestObject selectParentProgram = findTestObject('Base/commanXpath')
			 selectParentProgram.findProperty('xpath').setValue("//*[text()='Parent Program']/following::select[1]/option[text()='"+parentProgram+"']")
			 WebUI.doubleClick(selectParentProgram,FailureHandling.STOP_ON_FAILURE)
			 WebUI.delay(5)
			
			 //Click Program Name
			 TestObject selectProgramName = findTestObject('Base/commanXpath')
			 selectProgramName.findProperty('xpath').setValue("//*[text()='Program Name']/following::select[1]/option[text()='"+programName+"']")
			 WebUI.doubleClick(selectProgramName, FailureHandling.STOP_ON_FAILURE)
			 WebUI.delay(5)
			 
			 //Click Program Status
			 TestObject selectProgramStatus = findTestObject('Base/commanXpath')
			 selectProgramStatus.findProperty('xpath').setValue("//*[text()='Program Status']/following::select[1]/option[text()='"+programStatus+"']")
			 WebUI.doubleClick(selectProgramStatus, FailureHandling.STOP_ON_FAILURE)
			 WebUI.delay(5)
			 
			 //Click Organization
			 TestObject selectOrganization = findTestObject('Base/commanXpath')
			 selectOrganization.findProperty('xpath').setValue("//*[contains(text(),'Organization')]/following::select[1]/option[text()='"+organization+"']")
			 WebUI.doubleClick(selectOrganization, FailureHandling.STOP_ON_FAILURE)
			 WebUI.delay(2)
			 
			 //Click Primary Prayer Class
			 TestObject selectPrimaryPayerClass = findTestObject('Base/commanXpath')
			 selectPrimaryPayerClass.findProperty('xpath').setValue("//*[text()='Primary Payer Class']/following::select[1]/option[text()='"+primaryPayerClass+"']")
			 WebUI.doubleClick(selectPrimaryPayerClass, FailureHandling.STOP_ON_FAILURE)
			 WebUI.delay(2)
			 
			 //Click Secondary Prayer Class
			 TestObject selectSecondaryPayerClass = findTestObject('Base/commanXpath')
			 selectSecondaryPayerClass.findProperty('xpath').setValue("//*[text()='Secondary Payer Class']/following::select[1]/option[text()='"+secondaryPayerClass+"']")
			 WebUI.doubleClick(selectSecondaryPayerClass, FailureHandling.STOP_ON_FAILURE)
			 WebUI.delay(2)
			 
			 WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/View Report '), 30, FailureHandling.STOP_ON_FAILURE)
			 WebUI.doubleClick(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/View Report '))
			 WebUI.delay(200)
			 
			 WebUI.switchToFrame(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_reportContent'), 30,FailureHandling.STOP_ON_FAILURE)
			 WebUI.delay(3)
			 WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Patient Programming Information verification'), 500, FailureHandling.STOP_ON_FAILURE)
			 
			 String a = WebUI.getText(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Program Enrollment Verification'), FailureHandling.STOP_ON_FAILURE)
			 println " Patient from the view Report " + a
			 verifyStatus = StringUtils.isNumeric(a)
			 println " Verify Status " + verifyStatus
			 WebUI.delay(5)
			 WebUI.closeWindowIndex(1)
			 WebUI.switchToDefaultContent()

GlobalVariable.runStatus=WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/homePageVerification"), 20, FailureHandling.OPTIONAL)
test.log(LogStatus.INFO,"TestCase is executed successfully")
}

}catch(StepFailedException e){
test.log(LogStatus.FAIL, e.message)
CustomKeywords.'reports.extentReports.takeScreenshot'(test)
throw e
}